<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bg_BG">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../aboutdlg.ui" line="20"/>
        <source>About CoolReaderNG/Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdlg.ui" line="58"/>
        <source>About</source>
        <translation>За програмата</translation>
    </message>
    <message>
        <location filename="../aboutdlg.ui" line="79"/>
        <source>License</source>
        <translation>Лиценз</translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="44"/>
        <source>CoolReaderNG is free open source e-book viewer based on crengine-ng library.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="45"/>
        <source>Source code is available at</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="46"/>
        <source>under the terms of GNU GPL license either version 2 or (at your option) any later version.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="48"/>
        <source>It is a fork of the &apos;CoolReader&apos; program.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="50"/>
        <source>Third party components used in crengine-ng:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="53"/>
        <source>FreeType - font rendering</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="56"/>
        <source>HarfBuzz - text shaping, font kerning, ligatures</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="59"/>
        <source>ZLib - compressing library</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="62"/>
        <source>ZSTD - compressing library</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="65"/>
        <source>libpng - PNG image format support</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="68"/>
        <source>libjpeg - JPEG image format support</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="71"/>
        <source>FriBiDi - RTL writing support</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="74"/>
        <source>libunibreak - line breaking and word breaking algorithms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="77"/>
        <source>utf8proc - for unicode string comparision</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="80"/>
        <source>NanoSVG - SVG image format support</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="83"/>
        <source>chmlib - chm format support</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="86"/>
        <source>antiword - Microsoft Word format support</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="89"/>
        <source>RFC6234 (sources) - SHAsum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="92"/>
        <source>cmark - CommonMark parsing and rendering library and program in C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="96"/>
        <source>cmark-gfm - GitHub&apos;s fork of cmark, a CommonMark parsing and rendering library and program in C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="99"/>
        <source>hyphman - AlReader hyphenation manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="100"/>
        <source>Most hyphenation dictionaries - TEX hyphenation patterns</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="101"/>
        <source>Russian hyphenation dictionary - </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../aboutdlg.cpp" line="104"/>
        <source>Languages character set database by Fontconfig</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AddBookmarkDialog</name>
    <message>
        <location filename="../addbookmarkdlg.ui" line="66"/>
        <source>Title</source>
        <translation>Заглавие</translation>
    </message>
    <message>
        <location filename="../addbookmarkdlg.ui" line="22"/>
        <source>Bookmark type</source>
        <translation>Тип отметки</translation>
    </message>
    <message>
        <location filename="../addbookmarkdlg.cpp" line="49"/>
        <source>Add bookmark</source>
        <translation>Добавяне на отметка</translation>
    </message>
    <message>
        <location filename="../addbookmarkdlg.ui" line="46"/>
        <source>Position text</source>
        <translation>Текст</translation>
    </message>
    <message>
        <location filename="../addbookmarkdlg.ui" line="32"/>
        <location filename="../addbookmarkdlg.cpp" line="54"/>
        <source>Position</source>
        <translation>Позиция</translation>
    </message>
    <message>
        <location filename="../addbookmarkdlg.cpp" line="57"/>
        <location filename="../addbookmarkdlg.cpp" line="126"/>
        <source>Correction</source>
        <translation>Корекция</translation>
    </message>
    <message>
        <location filename="../addbookmarkdlg.ui" line="53"/>
        <location filename="../addbookmarkdlg.cpp" line="56"/>
        <location filename="../addbookmarkdlg.cpp" line="122"/>
        <source>Comment</source>
        <translation>Коментар</translation>
    </message>
    <message>
        <location filename="../addbookmarkdlg.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../addbookmarkdlg.ui" line="39"/>
        <source>Page 1/10 10%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../addbookmarkdlg.ui" line="73"/>
        <source>sample title</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BookmarkListDialog</name>
    <message>
        <location filename="../bookmarklistdlg.cpp" line="50"/>
        <source>Text</source>
        <translation>Текст</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.ui" line="78"/>
        <source>Close</source>
        <translation>Изход</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.ui" line="65"/>
        <source>Remove ALL Bookmarks</source>
        <translation>Премахване на всички отметки</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.ui" line="57"/>
        <source>Remove Bookmark</source>
        <translation>Премахване на отметката</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.ui" line="49"/>
        <source>Go to Bookmark</source>
        <translation>Преминаване на отметка</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.cpp" line="46"/>
        <source>Bookmarks</source>
        <translation>Отметки</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.cpp" line="50"/>
        <source>Position</source>
        <translation>Позиция</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.cpp" line="51"/>
        <source>Comment</source>
        <translation>Коментар</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.ui" line="70"/>
        <source>Edit Bookmark</source>
        <translation>Редактиране на отметка</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.ui" line="52"/>
        <source>Return</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.ui" line="60"/>
        <source>Del</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.ui" line="73"/>
        <source>F4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.ui" line="81"/>
        <source>Esc</source>
        <translation type="unfinished">Esc</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.cpp" line="50"/>
        <source>Type</source>
        <comment>bookmark type</comment>
        <translation type="unfinished">Тип</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.cpp" line="88"/>
        <source>P</source>
        <comment>Bookmark type first letter - Position</comment>
        <translation type="unfinished">П</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.cpp" line="90"/>
        <source>C</source>
        <comment>Bookmark type first letter - Comment</comment>
        <translation type="unfinished">К</translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.cpp" line="92"/>
        <source>E</source>
        <comment>Bookmark type first letter - Correction/Edit</comment>
        <translation type="unfinished">К</translation>
    </message>
</context>
<context>
    <name>CR3View</name>
    <message>
        <location filename="../cr3widget.cpp" line="1329"/>
        <source>Error while opening document </source>
        <translation>Възникна грешка при отваряне на документа</translation>
    </message>
    <message>
        <location filename="../cr3widget.cpp" line="1683"/>
        <source>Loading: please wait...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cr3widget.cpp" line="1223"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cr3widget.cpp" line="1224"/>
        <source>Font &quot;%1&quot; isn&apos;t compatible with language &quot;%2&quot;. Instead will be used fallback font.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExportProgressDlg</name>
    <message>
        <location filename="../exportprogressdlg.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../exportprogressdlg.ui" line="20"/>
        <source>Export is in progress...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FallbackFontsDialog</name>
    <message>
        <location filename="../fallbackfontsdialog.ui" line="14"/>
        <source>Fallback fonts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../fallbackfontsdialog.ui" line="23"/>
        <source>List of fallback fonts:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../fallbackfontsdialog.cpp" line="163"/>
        <source>Remove this fallback font</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FilePropsDialog</name>
    <message>
        <location filename="../filepropsdlg.cpp" line="212"/>
        <source>Date</source>
        <translation>Дата</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="229"/>
        <source>ISBN</source>
        <translation>ISBN</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="208"/>
        <source>Title</source>
        <translation>Заглавие</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="44"/>
        <source>Value</source>
        <translation>Стойност</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="194"/>
        <source>Current chapter</source>
        <translation>Глава</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="222"/>
        <source>Document version</source>
        <translation>Версия на документа</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="211"/>
        <source>Series number</source>
        <translation>Пореден номер</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="189"/>
        <source>Current Time</source>
        <translation>Текущо време</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="186"/>
        <source>Current page</source>
        <translation>Текуща страница</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="206"/>
        <source>File info</source>
        <translation>Информация за файла</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="202"/>
        <source>File name</source>
        <translation>Име на файл</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="203"/>
        <source>File path</source>
        <translation>Път до файл</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="204"/>
        <source>File size</source>
        <translation>Размер на файла</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="188"/>
        <source>Battery state</source>
        <translation>Състояние на батарията</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="213"/>
        <source>Genres</source>
        <translation>Жанрове</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="221"/>
        <source>OCR by</source>
        <translation>OCR</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="215"/>
        <source>Translator</source>
        <translation>Преводач</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="197"/>
        <source>Status</source>
        <translation>Статус</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="232"/>
        <source>Custom info</source>
        <translation>Друга информация</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="216"/>
        <source>Book info</source>
        <translation>Информация за книгата</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="205"/>
        <source>File format</source>
        <translation>Файлов формат</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="220"/>
        <source>Document source URL</source>
        <translation>URL на изходния документ</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="227"/>
        <source>Publisher city</source>
        <translation>Град</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="41"/>
        <source>Document properties</source>
        <translation>Свойства на документа</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="230"/>
        <source>Publication info</source>
        <translation>Информация за публикацията</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="228"/>
        <source>Publication year</source>
        <translation>Година на издаване</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="225"/>
        <source>Publication name</source>
        <translation>Име на публикацията</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="201"/>
        <source>Archive size</source>
        <translation>Размер на архива</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="199"/>
        <source>Archive name</source>
        <translation>Име на архива</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="200"/>
        <source>Archive path</source>
        <translation>Път до архива</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="226"/>
        <source>Publisher</source>
        <translation>Издателство</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="44"/>
        <source>Property</source>
        <translation>Свойство</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="209"/>
        <source>Author(s)</source>
        <translation>Автор(и)</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="210"/>
        <source>Series name</source>
        <translation>Серия</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="218"/>
        <source>Document author</source>
        <translation>Автор на документа</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="187"/>
        <source>Total pages</source>
        <translation>Общо страници</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="223"/>
        <source>Document info</source>
        <translation>Информация за документа</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="219"/>
        <source>Document date</source>
        <translation>Дата на документа</translation>
    </message>
    <message>
        <location filename="../filepropsdlg.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="214"/>
        <source>Language</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FontFamiliesDialog</name>
    <message>
        <location filename="../fontfamiliesdialog.ui" line="23"/>
        <source>Generic font families</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../fontfamiliesdialog.ui" line="36"/>
        <source>Generic font families settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../fontfamiliesdialog.ui" line="45"/>
        <source>Serif</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../fontfamiliesdialog.ui" line="62"/>
        <source>Sans-Serif</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../fontfamiliesdialog.ui" line="79"/>
        <source>Cursive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../fontfamiliesdialog.ui" line="96"/>
        <source>Fantasy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../fontfamiliesdialog.ui" line="113"/>
        <source>Monospace</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="346"/>
        <source>Open book file</source>
        <translation>Отваряне на книга</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="287"/>
        <source>Export document to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="187"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="187"/>
        <source>The maximum number of tabs has been exceeded!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="287"/>
        <source>WOL book (*.wol)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="292"/>
        <source>Export to WOL format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="348"/>
        <source>All supported formats</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="349"/>
        <source>FB2 books</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="351"/>
        <source>Text files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="352"/>
        <source>Rich text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="353"/>
        <source>MS Word document</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="355"/>
        <source>HTML files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="357"/>
        <source>Markdown files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="359"/>
        <source>EPUB files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="361"/>
        <source>CHM files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="363"/>
        <source>MOBI files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="364"/>
        <source>PalmDOC files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="365"/>
        <source>ZIP archives</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="350"/>
        <source>FB3 books</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="354"/>
        <source>Open Document files</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindowClass</name>
    <message>
        <location filename="../mainwindow.ui" line="280"/>
        <source>End</source>
        <translation>End</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="203"/>
        <source>Esc</source>
        <translation>Esc</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="289"/>
        <source>Back</source>
        <translation>Назад</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="434"/>
        <source>Copy</source>
        <translation>Копиране</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="78"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="125"/>
        <source>Help</source>
        <translation>Помощ</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="91"/>
        <source>View</source>
        <translation>Изглед</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="419"/>
        <source>Settings...</source>
        <translation>Настройки...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="192"/>
        <source>Close</source>
        <translation>Изход</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="200"/>
        <source>Minimize</source>
        <translation>Минимизиране</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="491"/>
        <source>Bookmark List...</source>
        <translation>Списък с отметки...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="359"/>
        <source>Zoom In</source>
        <translation>Увеличаване размера на шрифта</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="266"/>
        <source>Go to first page</source>
        <translation>Към първа страница</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="476"/>
        <source>Add Bookmark</source>
        <translation>Добавяне на отметка</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="479"/>
        <source>Add bookmark</source>
        <translation>Добавяне на отметка</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="252"/>
        <source>Line Up</source>
        <translation>Един ред нагоре</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="404"/>
        <source>Recent Books</source>
        <translation>Последни книги</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="536"/>
        <source>Find text</source>
        <translation>Търсене на текст</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="241"/>
        <source>Line Down</source>
        <translation>Един ред надолу</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="552"/>
        <source>Export</source>
        <translation>Изнасяне</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="326"/>
        <source>Previous Chapter</source>
        <translation>Предишна глава</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="389"/>
        <source>Table of Contents...</source>
        <translation>Съдържание...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="521"/>
        <source>Rotate</source>
        <translation>Завъртане</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="437"/>
        <source>Copy selected text</source>
        <translation>Копиране на избрания текст</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="374"/>
        <source>Zoom Out</source>
        <translation>Умаляване размера на шрифта</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="169"/>
        <source>Open...</source>
        <translation>Отваряне...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="230"/>
        <source>Page Up</source>
        <translation>Предишна страница</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="344"/>
        <source>Toggle Full Screen</source>
        <translation>Цял екран</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="347"/>
        <source>Toggle Full Screen mode</source>
        <translation>Цял екран</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="218"/>
        <source>Go to next page</source>
        <translation>Следваща страница</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="175"/>
        <source>Ctrl+O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="183"/>
        <source>Ctrl+T</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="195"/>
        <source>Ctrl+Q</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="362"/>
        <source>Increase font size</source>
        <translation>Увеличаване размера на шрифта</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="377"/>
        <source>Decrease font size</source>
        <translation>Умаляване размера на шрифта</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="456"/>
        <location filename="../mainwindow.ui" line="459"/>
        <source>About Qt</source>
        <translation>Относно Qt</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="506"/>
        <source>File Properties...</source>
        <translation>Свойства...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="263"/>
        <source>First Page</source>
        <translation>Първа страница</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="274"/>
        <source>Last Page</source>
        <translation>Последна страница</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="180"/>
        <source>Toggle Pages/Scroll</source>
        <translation>Страница/Свитък</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="509"/>
        <source>Show file properties</source>
        <translation>Свойства</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="172"/>
        <source>Open file</source>
        <translation>Отваряне на файл</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="392"/>
        <source>Show table of contents</source>
        <translation>Съдържание</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="315"/>
        <source>Next Chapter</source>
        <translation>Следваща глава</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="422"/>
        <source>Settings dialog</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="233"/>
        <source>Back by page</source>
        <translation>Предишна страница</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="304"/>
        <source>Forward</source>
        <translation>Напред</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="215"/>
        <source>Page Down</source>
        <translation>Следваща страница</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="533"/>
        <source>Find text...</source>
        <translation>Търсене на текст...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="407"/>
        <source>Show recent books list</source>
        <translation>Показване на списък с последните книги</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="109"/>
        <source>Navigation</source>
        <translation>Навигация</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="494"/>
        <source>Show bookmarks list</source>
        <translation>Показване на списък с отметки</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="221"/>
        <source>PgDown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="236"/>
        <source>PgUp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="244"/>
        <source>Forward by one line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="247"/>
        <source>Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="255"/>
        <source>Back by line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="258"/>
        <source>Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="269"/>
        <source>Home</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="277"/>
        <source>Go to last page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="292"/>
        <source>Back in navigation history</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="295"/>
        <source>Backspace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="307"/>
        <source>Go to the next position in navigation history</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="310"/>
        <source>Shift+Backspace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="318"/>
        <source>Go to next chapter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="321"/>
        <source>Alt+Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="329"/>
        <source>Go to previous chapter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="332"/>
        <source>Alt+Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="350"/>
        <source>Alt+Return</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="365"/>
        <source>+</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="380"/>
        <source>-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="395"/>
        <source>F4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="410"/>
        <source>F5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="425"/>
        <source>F9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="440"/>
        <source>Ctrl+C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="445"/>
        <source>copy2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="448"/>
        <source>Copy alternative shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="451"/>
        <source>Ctrl+Ins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="482"/>
        <source>Ctrl+B</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="497"/>
        <source>F6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="512"/>
        <source>Ctrl+F1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="524"/>
        <source>Ctrl+R</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="539"/>
        <source>Ctrl+F</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="544"/>
        <source>Toggle Edit Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="547"/>
        <source>Toggle edit mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="555"/>
        <source>Export document</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="558"/>
        <source>Ctrl+S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="563"/>
        <location filename="../mainwindow.ui" line="566"/>
        <location filename="../mainwindow.ui" line="585"/>
        <location filename="../mainwindow.ui" line="588"/>
        <source>Next Page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="569"/>
        <source>Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="574"/>
        <location filename="../mainwindow.ui" line="577"/>
        <source>Previous Page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="580"/>
        <source>Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="591"/>
        <source>Space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="596"/>
        <source>Next Sentence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="599"/>
        <source>Move selection to next sentence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="602"/>
        <source>Ctrl+Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="613"/>
        <source>Ctrl+Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="622"/>
        <source>New tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="625"/>
        <source>Open new tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="14"/>
        <source>CoolReaderNG/Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="464"/>
        <location filename="../mainwindow.ui" line="467"/>
        <source>About CoolReaderNG/Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="607"/>
        <source>Prev Sentence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="610"/>
        <source>Select previous sentence</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecentBooksDlg</name>
    <message>
        <location filename="../recentdlg.cpp" line="44"/>
        <source>Filename</source>
        <translation>Име на файл</translation>
    </message>
    <message>
        <location filename="../recentdlg.cpp" line="44"/>
        <source>Title</source>
        <translation>Заглавие</translation>
    </message>
    <message>
        <location filename="../recentdlg.ui" line="14"/>
        <source>Recent Books</source>
        <translation>Последни книги</translation>
    </message>
    <message>
        <location filename="../recentdlg.cpp" line="44"/>
        <source>Author</source>
        <translation>Автор</translation>
    </message>
    <message>
        <location filename="../recentdlg.cpp" line="177"/>
        <source>Do you really want to remove all history records?</source>
        <translation>Действително ли желаете да премахнете всички записи от списъка с последните отворени книги?</translation>
    </message>
    <message>
        <location filename="../recentdlg.cpp" line="176"/>
        <source>Remove all history items</source>
        <translation>Изтриване списъка с последните отворени книги</translation>
    </message>
    <message>
        <location filename="../recentdlg.ui" line="49"/>
        <source>Clear All</source>
        <translation>Изтриване на всичко</translation>
    </message>
    <message>
        <location filename="../recentdlg.ui" line="41"/>
        <source>Remove Item</source>
        <translation>Изтриване на записа</translation>
    </message>
    <message>
        <location filename="../recentdlg.ui" line="44"/>
        <source>Del</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../recentdlg.cpp" line="44"/>
        <source>#</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SampleView</name>
    <message>
        <location filename="../sampleview.cpp" line="30"/>
        <source>Style Preview</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SearchDialog</name>
    <message>
        <location filename="../searchdlg.ui" line="14"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../searchdlg.ui" line="25"/>
        <source>Text</source>
        <translation type="unfinished">Текст</translation>
    </message>
    <message>
        <location filename="../searchdlg.ui" line="32"/>
        <source>Case Sensitive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../searchdlg.ui" line="39"/>
        <source>Search forward</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../searchdlg.ui" line="46"/>
        <source>Search backward</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../searchdlg.ui" line="73"/>
        <source>Find Next</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../searchdlg.ui" line="83"/>
        <source>Close</source>
        <translation type="unfinished">Изход</translation>
    </message>
    <message>
        <location filename="../searchdlg.cpp" line="148"/>
        <source>Not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../searchdlg.cpp" line="149"/>
        <source>Search pattern is not found in document</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsDlg</name>
    <message>
        <location filename="../settings.ui" line="141"/>
        <source>Page</source>
        <translation>Страница</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="473"/>
        <source>Clock</source>
        <translation>Час</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="101"/>
        <source>Show toolbar</source>
        <translation>Показване на панела с инструменти</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="166"/>
        <source>Two pages</source>
        <translation>Две страници</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="171"/>
        <source>Scroll View</source>
        <translation>Свитък</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="51"/>
        <source>Look &amp; feel</source>
        <translation>Визуален стил</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="406"/>
        <source>[No hyphenation]</source>
        <translation>[Без сричкопренасяне]</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1699"/>
        <source>Interline spacing</source>
        <translation>Интервал между редовете</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="161"/>
        <source>One page</source>
        <translation>Една страница</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="564"/>
        <location filename="../settings.ui" line="632"/>
        <location filename="../settings.ui" line="687"/>
        <location filename="../settings.ui" line="742"/>
        <location filename="../settings.ui" line="1082"/>
        <location filename="../settings.ui" line="1549"/>
        <source>Change</source>
        <translation>Промяна</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="115"/>
        <source>Show scroll bar</source>
        <translation>Показване на лентата за превъртане</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="69"/>
        <source>Open recent book</source>
        <translation>Отвори последната книга</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="133"/>
        <source>Fullscreen display</source>
        <translation>Режим на цял екран</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="84"/>
        <source>Do nothing</source>
        <translation>Не прави нищо</translation>
    </message>
    <message>
        <source>Sample</source>
        <translation type="vanished">Пример</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1056"/>
        <source>Styles</source>
        <translation>Стилове</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="365"/>
        <source>Book name</source>
        <translation>Заглавие</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="43"/>
        <source>Window</source>
        <translation>Прозорец</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="37"/>
        <source>Window options</source>
        <translation>Прозорец</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="92"/>
        <source>Controls</source>
        <translation>Управление</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1067"/>
        <location filename="../settings.cpp" line="1013"/>
        <source>Text color</source>
        <translation>Цвят на текста</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1789"/>
        <source>.TXT files</source>
        <translation>.TXT файлове</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="108"/>
        <source>Show menu</source>
        <translation>Показване на менюто</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="198"/>
        <source>Page margins</source>
        <translation>Бели полета</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="79"/>
        <source>Show File Open dialog</source>
        <translation>Показване диалог за отваряне на файл</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1534"/>
        <location filename="../settings.cpp" line="1017"/>
        <source>Background color</source>
        <translation>Цвят на фона</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1796"/>
        <source>Disable automatic formatting</source>
        <translation>Изключване на автоформатирането</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="955"/>
        <source>The quick brown fox jumps over the lazy dog. </source>
        <translation>Вкъщи не яж сьомга с фиде без ракийка и хапка люта чушчица.</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="74"/>
        <source>Show list of recent books</source>
        <translation>Показване на списъка с последните книги</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="532"/>
        <source>Header font</source>
        <translation>Шрифт на колонтитула</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1661"/>
        <location filename="../settings.ui" line="1675"/>
        <source>Hyphenation</source>
        <translation>Сричкопренасяне</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="185"/>
        <source>Vew Mode</source>
        <translation>Режим за преглеждане</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="23"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="61"/>
        <source>Startup action</source>
        <translation>Действие при стартиране</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="549"/>
        <location filename="../settings.cpp" line="1021"/>
        <source>Page header text color</source>
        <translation>Цвят на колонтитула</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="122"/>
        <source>Show status bar</source>
        <translation>Показване на лентата за състояние</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="408"/>
        <source>[Algorythmic hyphenation]</source>
        <translation>[Алгоритмично сричкопренасяне]</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1499"/>
        <source>Page skin</source>
        <translation>Стил на фона</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="500"/>
        <source>Battery status</source>
        <translation>Състояние на батарията</translation>
    </message>
    <message>
        <source>Text font</source>
        <translation type="vanished">Шрифт</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="608"/>
        <source>Show footnotes at bottom of page</source>
        <translation>Показване на бележките под линия в края на страницата</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="319"/>
        <source>Show page header</source>
        <translation>Показване на колонтитул</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="206"/>
        <location filename="../settings.ui" line="844"/>
        <location filename="../settings.ui" line="891"/>
        <location filename="../settings.ui" line="945"/>
        <location filename="../settings.ui" line="999"/>
        <location filename="../settings.cpp" line="556"/>
        <location filename="../settings.cpp" line="597"/>
        <source>0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="211"/>
        <location filename="../settings.ui" line="849"/>
        <location filename="../settings.ui" line="896"/>
        <location filename="../settings.ui" line="950"/>
        <location filename="../settings.ui" line="1004"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="216"/>
        <location filename="../settings.ui" line="854"/>
        <location filename="../settings.ui" line="901"/>
        <location filename="../settings.ui" line="955"/>
        <location filename="../settings.ui" line="1009"/>
        <source>2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="221"/>
        <location filename="../settings.ui" line="859"/>
        <location filename="../settings.ui" line="906"/>
        <location filename="../settings.ui" line="960"/>
        <location filename="../settings.ui" line="1014"/>
        <source>3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="226"/>
        <source>4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="231"/>
        <source>5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="236"/>
        <source>6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="241"/>
        <source>7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="246"/>
        <source>8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="251"/>
        <source>9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="256"/>
        <source>10</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="261"/>
        <source>11</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="266"/>
        <source>12</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="271"/>
        <source>15</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="276"/>
        <source>20</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="281"/>
        <source>25</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="286"/>
        <source>30</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="291"/>
        <source>40</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="296"/>
        <source>50</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="301"/>
        <source>60</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="446"/>
        <source>Position percent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="617"/>
        <location filename="../settings.cpp" line="1262"/>
        <source>Selection color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="672"/>
        <source>Comment color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="727"/>
        <source>Correction color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="782"/>
        <source>Bookmark highlight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="790"/>
        <location filename="../settings.ui" line="825"/>
        <location filename="../settings.ui" line="872"/>
        <location filename="../settings.ui" line="926"/>
        <location filename="../settings.ui" line="980"/>
        <source>Disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="795"/>
        <source>Solid fill</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="800"/>
        <location filename="../settings.cpp" line="783"/>
        <source>Underline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="815"/>
        <source>Inline images zoom in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="830"/>
        <location filename="../settings.ui" line="877"/>
        <location filename="../settings.ui" line="931"/>
        <location filename="../settings.ui" line="985"/>
        <source>Integer scale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="835"/>
        <location filename="../settings.ui" line="882"/>
        <location filename="../settings.ui" line="936"/>
        <location filename="../settings.ui" line="990"/>
        <source>Arbitrary scale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="916"/>
        <source>Block images zoom in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="970"/>
        <source>Block images zoom out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1024"/>
        <source>Inline images zoom out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1031"/>
        <source>Image scaling options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1140"/>
        <source>Default font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1222"/>
        <source>Font gamma</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1415"/>
        <source>Font antialiasing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1492"/>
        <location filename="../settings.ui" line="1803"/>
        <source>Manage...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1685"/>
        <source>Text shaping</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1733"/>
        <source>Floating punctuation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1740"/>
        <source>Enable floating punctuation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1257"/>
        <source>0.3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="392"/>
        <source>Page number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="419"/>
        <source>Page count</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1262"/>
        <source>0.35</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1267"/>
        <source>0.4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1272"/>
        <source>0.45</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1277"/>
        <source>0.5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1282"/>
        <source>0.55</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1287"/>
        <source>0.6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1292"/>
        <source>0.65</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1297"/>
        <source>0.7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1302"/>
        <source>0.75</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1307"/>
        <source>0.8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1312"/>
        <source>0.85</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1317"/>
        <source>0.9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1322"/>
        <source>0.95</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1327"/>
        <source>0.98</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1332"/>
        <source>1.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1337"/>
        <source>1.02</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1342"/>
        <source>1.05</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1347"/>
        <source>1.1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1352"/>
        <source>1.15</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1357"/>
        <source>1.2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1362"/>
        <source>1.25</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1367"/>
        <source>1.3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1372"/>
        <source>1.35</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1377"/>
        <source>1.4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1382"/>
        <source>1.45</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1387"/>
        <source>1.5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1392"/>
        <source>1.6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1397"/>
        <source>1.7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1402"/>
        <source>1.8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1407"/>
        <source>1.9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1429"/>
        <source>Font hinting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1461"/>
        <source>No hinting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1466"/>
        <source>Use bytecode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1471"/>
        <source>Autohinting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1479"/>
        <source>Fallback fonts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1754"/>
        <source>Enable document internal styles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1747"/>
        <source>Internal CSS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1768"/>
        <source>Enable document embedded fonts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1761"/>
        <source>.EPUB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1829"/>
        <location filename="../settings.cpp" line="433"/>
        <source>Default paragraph style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1863"/>
        <source>Alignment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1873"/>
        <source>First line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1883"/>
        <source>Font size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1893"/>
        <source>Font face</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1205"/>
        <location filename="../settings.ui" line="1903"/>
        <source>Font weight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1709"/>
        <source>Min space width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1810"/>
        <source>Font families</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1913"/>
        <source>Font style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1923"/>
        <source>Font color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1933"/>
        <source>Margins:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1940"/>
        <source>Before</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1950"/>
        <source>After</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1960"/>
        <location filename="../settings.cpp" line="510"/>
        <source>Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1970"/>
        <location filename="../settings.cpp" line="510"/>
        <source>Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1983"/>
        <source>Interline space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1993"/>
        <source>Text decoration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2003"/>
        <source>Vertical align</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1820"/>
        <source>Stylesheet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="434"/>
        <source>Title</source>
        <translation type="unfinished">Заглавие</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="435"/>
        <source>Subtitle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="436"/>
        <source>Preformatted text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="437"/>
        <source>Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="438"/>
        <source>Cite / quotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="439"/>
        <source>Epigraph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="440"/>
        <source>Poem</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="441"/>
        <source>Text author</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="442"/>
        <source>Footnote link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="443"/>
        <source>Footnote</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="444"/>
        <source>Footnote title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="445"/>
        <source>Annotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="510"/>
        <location filename="../settings.cpp" line="526"/>
        <location filename="../settings.cpp" line="555"/>
        <location filename="../settings.cpp" line="596"/>
        <location filename="../settings.cpp" line="629"/>
        <location filename="../settings.cpp" line="651"/>
        <location filename="../settings.cpp" line="670"/>
        <location filename="../settings.cpp" line="716"/>
        <location filename="../settings.cpp" line="781"/>
        <location filename="../settings.cpp" line="799"/>
        <source>-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="510"/>
        <source>Justify</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="510"/>
        <source>Center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="526"/>
        <source>No indent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="526"/>
        <source>Small Indent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="526"/>
        <source>Big Indent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="526"/>
        <source>Small Outdent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="526"/>
        <source>Big Outdent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="557"/>
        <source>20% of line height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="558"/>
        <source>30% of line height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="559"/>
        <location filename="../settings.cpp" line="598"/>
        <source>50% of line height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="560"/>
        <location filename="../settings.cpp" line="599"/>
        <source>100% of line height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="561"/>
        <location filename="../settings.cpp" line="600"/>
        <source>150% of line height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="601"/>
        <source>200% of line height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="602"/>
        <source>400% of line height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="603"/>
        <source>5% of line width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="604"/>
        <source>10% of line width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="605"/>
        <source>15% of line width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="606"/>
        <source>20% of line width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="607"/>
        <source>30% of line width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="630"/>
        <location filename="../settings.cpp" line="671"/>
        <source>Normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="631"/>
        <source>Bold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="632"/>
        <source>Bolder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="633"/>
        <source>Lighter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="652"/>
        <source>Increase: 110%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="653"/>
        <source>Increase: 120%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="654"/>
        <source>Increase: 150%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="655"/>
        <source>Decrease: 90%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="656"/>
        <source>Decrease: 80%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="657"/>
        <source>Decrease: 70%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="658"/>
        <source>Decrease: 60%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="672"/>
        <source>Italic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="681"/>
        <source>[Default Sans Serif]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="683"/>
        <source>[Default Serif]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="685"/>
        <source>[Default Monospace]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="717"/>
        <source>Black</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="718"/>
        <source>Green</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="719"/>
        <source>Silver</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="720"/>
        <source>Lime</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="232"/>
        <location filename="../settings.cpp" line="239"/>
        <location filename="../settings.cpp" line="721"/>
        <source>Gray</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="722"/>
        <source>Olive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="723"/>
        <source>White</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="724"/>
        <source>Yellow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="725"/>
        <source>Maroon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="726"/>
        <source>Navy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="727"/>
        <source>Red</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="728"/>
        <source>Blue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="729"/>
        <source>Purple</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="730"/>
        <source>Teal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="731"/>
        <source>Fuchsia</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="732"/>
        <source>Aqua</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="327"/>
        <location filename="../settings.cpp" line="231"/>
        <location filename="../settings.cpp" line="238"/>
        <location filename="../settings.cpp" line="782"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="784"/>
        <source>Line through</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="785"/>
        <source>Overline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="800"/>
        <source>Baseline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="801"/>
        <source>Subscript</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="802"/>
        <source>Superscript</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1266"/>
        <source>Comment bookmark color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1270"/>
        <source>Correction bookmark color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="332"/>
        <source>Page header</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="337"/>
        <source>Page footer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1595"/>
        <source>Rendering flags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1603"/>
        <location filename="../settings.ui" line="1634"/>
        <source>Legacy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1608"/>
        <source>Flat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1613"/>
        <source>Book</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1618"/>
        <source>Web (Full)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1626"/>
        <source>DOM level:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1639"/>
        <source>Newest</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1647"/>
        <source>Multi languages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1654"/>
        <source>Support for multilingual documents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="203"/>
        <source>Simple (FreeType only, fastest)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="204"/>
        <source>Light (HarfBuzz without ligatures)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="205"/>
        <source>Full (HarfBuzz with ligatures)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1719"/>
        <source>Font kerning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1726"/>
        <source>Enable font kerning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1782"/>
        <source>Ignore document margins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1668"/>
        <source>Enable hyphenation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="233"/>
        <source>LCD (RGB)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="234"/>
        <source>LCD (BGR)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="235"/>
        <source>LCD (RGB) vertical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="236"/>
        <source>LCD (BGR) vertical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1124"/>
        <source>synthetic*</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1126"/>
        <source>synthetic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1775"/>
        <source>Margins redefine</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TocDlg</name>
    <message>
        <location filename="../tocdlg.cpp" line="75"/>
        <source>Page</source>
        <translation>Страница</translation>
    </message>
    <message>
        <location filename="../tocdlg.cpp" line="75"/>
        <source>Title</source>
        <translation>Заглавие</translation>
    </message>
    <message>
        <location filename="../tocdlg.ui" line="14"/>
        <source>Table of Contents</source>
        <translation>Съдържание</translation>
    </message>
</context>
<context>
    <name>WolExportDlg</name>
    <message>
        <location filename="../wolexportdlg.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wolexportdlg.ui" line="22"/>
        <source>Bits per pixel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wolexportdlg.ui" line="30"/>
        <location filename="../wolexportdlg.ui" line="51"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wolexportdlg.ui" line="35"/>
        <location filename="../wolexportdlg.ui" line="56"/>
        <source>2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wolexportdlg.ui" line="43"/>
        <source>Table of Contents levels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wolexportdlg.ui" line="61"/>
        <source>3</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>crqtutils</name>
    <message>
        <location filename="../crqtutil.cpp" line="183"/>
        <source>Undetermined</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
